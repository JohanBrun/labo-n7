import pytest
from labo import *

@pytest.fixture
def lb():
    return {'A':'F307'}

def test_enregistrer_arivee(lb):
    """ enregistrer_arrivee(lab,'A', 'F307') """
    assert taille(lb) == 1
    assert lb['A'] == 'F307'

def test_enregistrer_depart(lb):
    try:
        enregistrer_depart(lb, 'X')
        assert False
    except AbsentException:
        pass

def test_modifier_bureau(lb):
    try:
        modifier_bureau(lb,'X','F306')
        assert False
    except AbsentException:
        pass

def test_modifier_bureau2(lb):
    """ enregistrer_arrivee(lab, 'A', 'F307') """
    modifier_bureau(lb, 'A', 'F306')
    assert lb['A'] == 'F306'

def test_corriger_nom(lb):
    try:
        corriger_nom(lb, 'X', 'B')
        assert False
    except AbsentException:
        pass

def test_corriger_nom2(lb):
    """ enregistrer_arrivee(lb, 'A', 'F307') """
    try:
        corriger_nom(lb, 'A', 'A')
        assert False
    except PresentException:
        pass

def test_corriger_nom3(lb):
    corriger_nom(lb, 'A', 'B')
    assert 'B' in lb.keys()

def test_membre_bureau(lb):
    assert membre_labo(lb, 'A') == True

def test_membre_bureau2(lb):
    assert not membre_labo(lb, 'X')
        

def test_bureau_personne(lb):
    try:
        bureau_personne(lb, 'X')
        assert False
    except AbsentException:
        pass

def test_bureau_personne2(lb):
    assert bureau_personne(lb, 'A') == 'F307'

def test_listing(lb):
    assert listing(lb) == lb



def test_charger_labo_json():
    try:
        charger_labo_json('existepas')
        assert False
    except FileErrorException:
        pass

def test_charger_labo_json2():
    assert charger_labo_json('Labo') == {"Anabelle": "F307", "Brigitte": "F307", "Celine": "F306", "Delphine": "F308", "Anastasia": "F306", "Charlotte": "F308", "Aurélie": "F306"}

def test_charger_labo_json3():
    assert charger_labo_json('Labo.json') == {"Anabelle": "F307", "Brigitte": "F307", "Celine": "F306", "Delphine": "F308", "Anastasia": "F306", "Charlotte": "F308", "Aurélie": "F306"}

def test_sauvegarder_labo_json(lb):
    sauvegarder_labo(lb, 'test_sauvegarde')
    assert charger_labo_json('test_sauvegarde') == {'A':'F307'}

def test_import_csv(lb):
    try:
        import_csv(lb, 'existepas')
        assert False
    except FileErrorException:
        pass

def test_import_csv2(lb):
    assert import_csv(lb, 'test2') 

def test_import_csv3(lb):
    assert import_csv(lb, 'test2.csv') 

def test_import_csv4(lb):
    assert import_csv(lb, 'test2') == {'A': ['F307', 'F306']}

def test_import_csv5(lb):
    import_csv(lb, 'test2')
    assert lb == {'A':'F306','M':'F305','X':'F305'}

def test_occupation_bureaux(lb):
    occupation_bureaux(lb)
    with open('occupation_bureaux.html', 'r') as fichier_occupation:
        lecture_fichier = fichier_occupation.read()
        assert lecture_fichier == '''
        <!DOCTYPE html>
        <head>
            <meta charset="utf-8">
            <meta name="Labo" content="occupation des bureaux">
            <title>Occupation des bureaux </title>
        </head>

        <body>
            <h1>Liste des bureaux occupés :</h1>
			<h2>F307 :</h2>
			<p>- A</p>

        </body>
        </html>'''

def test_occupation_bureaux2():
    lab = charger_labo_json('Labo')
    occupation_bureaux(lab)
    with open('occupation_bureaux.html', 'r') as fichier_occupation:
        lecture_fichier = fichier_occupation.read()
        assert lecture_fichier == '''
        <!DOCTYPE html>
        <head>
            <meta charset="utf-8">
            <meta name="Labo" content="occupation des bureaux">
            <title>Occupation des bureaux </title>
        </head>

        <body>
            <h1>Liste des bureaux occupés :</h1>
			<h2>F306 :</h2>
			<p>- Anastasia</p>
			<p>- Aurélie</p>
			<p>- Celine</p>
			<h2>F307 :</h2>
			<p>- Anabelle</p>
			<p>- Brigitte</p>
			<h2>F308 :</h2>
			<p>- Charlotte</p>
			<p>- Delphine</p>

        </body>
        </html>'''
            
