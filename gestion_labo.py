'''Module construisant le menu du labo et permettant les interactions utilisateurs'''
import labo
import menu

def gestion_labo():
    '''Crée le menu permettant de gérer le labo'''

    #Liste des commandes du menu
    def cmd_arrivee():
        try:
            nom = input("Nom : ")
            bureau = input("Bureau : ")
            labo.enregistrer_arrivee(lab, nom, bureau)
            print(f'{nom} est enregistré(e) dans le bureau {bureau}')
        except labo.PresentException:
            print('Personne déjà présente !')

    def cmd_depart():
        try:
            nom = input("Nom : ")
            labo.enregistrer_depart(lab, nom)
            print(f"{nom} a été supprimé(e)")
        except labo.AbsentException:
            print("Personne inconnue !")

    def cmd_modifier_bureau():
        try:
            nom = input("Nom : ")
            nouveau_bureau = input("Nouveau Bureau : ")
            labo.modifier_bureau(lab, nom, nouveau_bureau)
            print(f'{nom} a pour nouveau bureau : {nouveau_bureau}')
        except labo.AbsentException:
            print("Personne inconnue !")

    def cmd_corriger_nom():
        try:
            ancien_nom = input("Ancien Nom : ")
            nouveau_nom = input("Nouveau Nom : ")
            labo.corriger_nom(lab, ancien_nom, nouveau_nom)
            print(f'{ancien_nom} a été corrigé(e) en {nouveau_nom}')
        except labo.AbsentException:
            print('Ancien nom inconnu !')
        except labo.PresentException:
            print('Nouveau nom déjà existant !')

    def cmd_membre_labo():
        nom = input("Nom : ")
        if labo.membre_labo(lab, nom):
            print(f'{nom} est membre du laboratoire')
        else:
            print(f"{nom} n'est pas membre du laboratoire")

    def cmd_bureau_personne():
        try:
            nom = input("Nom : ")
            print(labo.bureau_personne(lab, nom))
        except labo.AbsentException:
            print("Personne inconnue !")

    def cmd_listing():
        print(labo.listing(lab))

    def cmd_occupation_bureaux():
        labo.occupation_bureaux(lab)
        print('Fichier occupation_bureaux.html créé !')

    def cmd_import_csv():
        try:
            nom_fichier = input("Nom Fichier : ")
            liste_nom_bureau = labo.import_csv(lab, nom_fichier)
            print(f"\nRapport Import CSV :")
            if len(liste_nom_bureau) > 0:
                for nom, bureau in liste_nom_bureau.items():
                    print(f"{nom} était déjà présent(e) dans le bureau {bureau[1]}. Suite à l'import son nouveau bureau est {bureau[0]}.")
            else:
                print(f"Aucun doublon dans l'import")
        except labo.FileErrorException:
            print('Fichier inconnu !')

    def cmd_sauvegarder_labo():
        nom_fichier = input("Nom Sauvegarde : ")
        labo.sauvegarder_labo(lab, nom_fichier)
        print(f'Fichier sauvegardé sous le nom {nom_fichier}.json')


    #ajout des fonctions au menu
    m = menu.menu()
    menu.ajouter_menu(m, "enregistrer l'arrivée d'une nouvelle personne", cmd_arrivee)
    menu.ajouter_menu(m, "enregistrer le départ d'une personne", cmd_depart)
    menu.ajouter_menu(m, "modifier le bureau occupé par une personne", cmd_modifier_bureau)
    menu.ajouter_menu(m, "changer le nom d'une personne du laboratoire", cmd_corriger_nom)
    menu.ajouter_menu(m, "savoir si une personne est membre du laboratoire", cmd_membre_labo)
    menu.ajouter_menu(m, "obtenir le bureau d'une personne", cmd_bureau_personne)
    menu.ajouter_menu(m, "produire le listing des personnels et de leur bureau", cmd_listing)
    menu.ajouter_menu(m, "afficher l'occupation des bureaux", cmd_occupation_bureaux)

    menu.ajouter_menu(m, "importer des donnees csv", cmd_import_csv)
    menu.ajouter_menu(m, "sauvegarder", cmd_sauvegarder_labo)


    #lance le menu
    menu.gerer_menu(m, "MENU LABO")




def lancement_labo():
    '''demarrage du labo avec choix de créer un nouveau labo ou d'en charger un préexistant'''

    #Liste des commandes du démarrage
    def cmd_nouveau_labo():
        global lab
        lab = labo.labo()
        gestion_labo()
        print(f'Nouveau Labo')

    def cmd_charger_labo_json():
        try:
            nom_fichier = input("Nom Sauvegarde : ")
            global lab
            lab = labo.charger_labo_json(nom_fichier)
            gestion_labo()
            print(f'Labo {nom_fichier}.json chargé')
        except labo.FileErrorException:
            print("Fichier inconnu !")

    #ajout des fonctions du menu
    demarrage = menu.menu()
    menu.ajouter_menu(demarrage, "nouveau labo", cmd_nouveau_labo)
    menu.ajouter_menu(demarrage, "charger un labo", cmd_charger_labo_json)

    #lance le menu
    menu.gerer_menu(demarrage, "DEMARRAGE LABO")

lancement_labo()
