import json, os, csv

#Initialisation des erreurs personnalisées du labo
class LaboException(Exception):
    """Généralise les exceptions du laboratoire."""
    pass
class AbsentException(LaboException):
    pass
class PresentException(LaboException):
    pass
class FileErrorException(LaboException):
    pass



#Fonction de création du Labo
def labo():
    ''' création d'un laboratoire sous la forme d'un dictionnaire'''
    return {}

def taille(labo):
    '''renvoi la taille du laboratoire'''
    return len(labo)

#Fonctions de sauvegarde et chargement de données

def sauvegarder_labo(labo, nom_fichier):
    '''sauvegarde le labo en cours dans un fichier json'''
    with open(nom_fichier+'.json', "w", encoding='utf-8') as save:
        json.dump(labo, save, indent=4)

def charger_labo_json(nom_fichier):
    '''importe une sauvegarde du laboratoire sous format json que l'extension soit spécifié par
    l'utilisateur ou non'''
    if not nom_fichier.endswith('.json'):
        nom_fichier = nom_fichier+'.json'
    if os.path.exists(nom_fichier) == False :
        raise FileErrorException
    with open(nom_fichier) as save:
        lab = json.load(save)
    return lab

def import_csv(labo, nom_fichier):
    '''importe les données d'un fichier csv et crée un rapport des différences'''
    liste_nom_bureau = {}
    if not nom_fichier.endswith('.csv'):
        nom_fichier = nom_fichier+'.csv'
    if os.path.exists(nom_fichier) == False :
        raise FileErrorException
    with open(nom_fichier) as csvfile:
        reader = csv.DictReader(csvfile, skipinitialspace = True)
        for row in reader:
            if row['Nom'] in labo.keys():
                if labo[row['Nom']] != row['Bureau']:
                    liste_nom_bureau[row['Nom']] = [labo[row['Nom']],row['Bureau']]
    
            labo[row['Nom']] = row['Bureau']
    return liste_nom_bureau

def occupation_bureaux(labo):
    '''crée un fichier occupation_bureaux.html dans lequel est enregistré la liste des bureaux du
    labo comportant la liste des personnes de chaque bureau.'''
    inv_labo = {}
    with open('occupation_bureaux.html', 'w') as fichier_occupation:
        debut_html = '''
        <!DOCTYPE html>
        <head>
            <meta charset="utf-8">
            <meta name="Labo" content="occupation des bureaux">
            <title>Occupation des bureaux </title>
        </head>

        <body>
            <h1>Liste des bureaux occupés :</h1>\n'''
        fin_html = '''
        </body>
        </html>'''
        fichier_occupation.write(debut_html)
        for nom, bureau in labo.items():
            inv_labo.setdefault(bureau, []).append(nom)
        liste_bureau = sorted(inv_labo.keys())
        for i in range (len(liste_bureau)):
            fichier_occupation.write(f'\t\t\t<h2>{liste_bureau[i]} :</h2>\n')
            liste_noms = list((inv_labo[liste_bureau[i]]))
            liste_noms = sorted(liste_noms)
            for j in range (len(liste_noms)):
                fichier_occupation.write(f'\t\t\t<p>- {liste_noms[j]}</p>\n')
        fichier_occupation.write(fin_html)


#Fonctions d'édition du Labo

def enregistrer_arrivee(labo, nom, bureau):
    '''enregistre une nouvelle entrée dans le laboratoire'''
    if nom in labo:
        raise PresentException
    labo[nom] = bureau

def enregistrer_depart(labo, nom):
    '''supprime une entrée du laboratoire'''
    if nom not in labo:
        raise AbsentException
    labo.pop(nom)

def modifier_bureau(labo, nom, nouveau_bureau):
    '''modifie la valeur bureau attachée à la clée nom'''
    if nom not in labo:
        raise AbsentException
    labo[nom] = nouveau_bureau
    return nom, nouveau_bureau

def corriger_nom(labo, ancien_nom, nouveau_nom):
    '''crée une nouvelle clée pour une valeur donnée'''
    if ancien_nom not in labo:
        raise AbsentException
    if nouveau_nom in labo:
        raise PresentException
    labo[nouveau_nom] = labo[ancien_nom]
    del labo[ancien_nom]

def membre_labo(labo, nom):
    '''renvoi la valeur vraie si la personne est membre du labo'''
    if nom in labo:
        return True
    
def bureau_personne(labo, nom):
    '''renvoi la valeur bureau attachée à la clée nom'''
    if nom not in labo:
        raise AbsentException
    return labo[nom]

def listing(labo):
    '''renvoi le dictionnaire'''
    return labo
        

                
