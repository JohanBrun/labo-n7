def menu():
    return []

def ajouter_menu(m, intitule, cmd):
    m.append((intitule, cmd))

def _afficher_menu(m, titre):
    print("\n", titre.upper(), sep='')
    for num, entree in enumerate(m, 1):
        intitule,_ = entree
        print(num, intitule)
    print(0, 'Quitter')

def _input_choix(m):
    choix = input()
    taille_max = len(m)
    while choix.isdigit() == False or int(choix) < 0 or int(choix) > taille_max:
        choix = input(f'choisir une option entre 0 et {taille_max}: ')
    return int(choix)

def _traiter_choix(m, choix):
    assert 0 <= choix
    assert choix <= len(m)
    if choix != 0:
        m[choix-1][1]()
    

def gerer_menu(m, titre):
    choix = None
    while choix != 0:
        _afficher_menu(m, titre)
        choix = _input_choix(m)
        _traiter_choix(m, choix)